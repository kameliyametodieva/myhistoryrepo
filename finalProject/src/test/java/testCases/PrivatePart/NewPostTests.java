package testCases.PrivatePart;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import testCases.BaseTest;
import org.junit.runners.MethodSorters;
import org.junit.FixMethodOrder;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class NewPostTests extends BaseTest {
    private static final NewPostPage newPostPage = new NewPostPage();
    private static final LogInPage logInPage = new LogInPage();
    private static final LogOutPage logOutPage = new LogOutPage();


    @BeforeClass
    public static void testClassInit() {
        logInPage.navigateToPage();
        logInPage.logIn(Utils.getUIMappingByKey("loginPage.userName"),
                Utils.getUIMappingByKey("loginPage.password"));
    }

    @Before
    public void beforTest() {
        newPostPage.navigateToPage();
        newPostPage.assertPageNavigated();
    }



    @Test
    public void SU_02_01_SuccessfulCreateNewPost() {

        newPostPage.createNewPublicPost(Utils.getUIMappingByKey("newPostPage.newText"));
        newPostPage.assertPostCreated();
    }

    @Test
    public void SU_02_12_SuccessfulCreatePublicPost() {
        newPostPage.assertPublicPostExists();
    }

    @Test
    public void SU_02_14_SuccessfulLikePost() {
        newPostPage.likePost();
        newPostPage.assertLikePost();
    }

    @Test
    public void SU_02_15_SuccessfulDislikePost() {
        newPostPage.dislikePost();
        newPostPage.assertDislikePost();
    }

    @Test
    public void SU_02_16_SuccessfulCreateNewComment() {
        newPostPage.CreateComment("good news");
        newPostPage.assertCommentCreated();
    }

    @Test
    public void SU_02_17_SuccessfulDeletePost() {
        newPostPage.DeleteNewPost();
        //newPostPage.assertPostDeleted();
//
    }
}
