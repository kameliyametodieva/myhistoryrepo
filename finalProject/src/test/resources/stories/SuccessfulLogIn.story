Meta:
@logIn

Narrative:
As a registered user
I want to log in my account
So that I can use the functionalities of the social network


Scenario: Successful login
Given Click navigation.Home element
Then Click login.SignInButton element
Then Type mi in loginPage.UsernameField field
Then Type aaa123AAA in loginPage.PasswordField field
And Click login.LoginButton element
Then Page navigated
