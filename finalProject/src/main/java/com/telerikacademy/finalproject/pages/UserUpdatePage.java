package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;

import java.util.concurrent.TimeUnit;

public class UserUpdatePage extends BasePage {
    public UserUpdatePage() {
        super("userUpdatePage.url");
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
    }

    public void updateFirstName(String firstName) {
        actions.isElementPresentUntilTimeout("userEditPage.firstName", 10);
        actions.clearField("userEditPage.firstName");
        actions.typeValueInField(firstName, "userEditPage.firstName");
    }

    public void updateLastName(String lastName) {
        actions.isElementPresentUntilTimeout("userEditPage.lastName", 10);
        actions.clearField("userEditPage.lastName");
        actions.typeValueInField(lastName, "userEditPage.lastName");
    }
    public void saveButton(){
        actions.clickElementAfterWait("userEditPage.updateMyProfileSave");
    }


    public void assertFirstNameIsChanged() {
        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("profilePage.updatedUserFirstName");
    }
    public void assertLastNameIsChanged() {
        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("profilePage.updatedUserLastName");
    }

//    @Override
//    public void updatePost(String editPostTitle) {
//
//    }
}







