//package testCases.PrivatePart;
//
//
//
//import com.telerikacademy.finalproject.pages.*;
//import com.telerikacademy.finalproject.utils.Utils;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import testCases.BaseTest;
//import org.junit.runners.MethodSorters;
//import org.junit.FixMethodOrder;
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//
//public class DeletePostTests extends BaseTest {
//    private static final DeletePostPage deletePostPage = new DeletePostPage();
//    private static final LogInPage logInPage = new LogInPage();
//    private static final LogOutPage logOutPage = new LogOutPage();
//
//
//    @BeforeClass
//    public static void testClassInit() {
//        logInPage.navigateToPage();
//        logInPage.logIn(Utils.getUIMappingByKey("loginPage.userName"),
//                Utils.getUIMappingByKey("loginPage.password"));
//
//        deletePostPage.navigateToPage();
//    }
//    @Test
//    public void SuccessfulDeletePost(){
//        deletePostPage.deletePost();
//        deletePostPage.assertPostDeleted("Post deleted successfully.");
//    }
//}
