//package com.telerikacademy.finalproject.pages;
//
//import com.telerikacademy.finalproject.pages.BasePage;
//import org.junit.Assert;
//import org.openqa.selenium.By;
//import org.openqa.selenium.support.ui.Select;
//
//import java.util.concurrent.TimeUnit;
//
//public class DeletePostPage extends BasePage {
//    public DeletePostPage() {
//        super("deletePost.url");
//        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
//    }
//
//
//    public void deletePost() {
////        actions.waitForElementVisible("deletePostPade.exploreThisPost", 50);
//        actions.clickElement("deletePostPade.exploreThisPost");
//        Select selectBox = new Select(driver.findElement(By.id("StringListId")));
//        selectBox.selectByVisibleText("Delete post");
//        actions.clickElementAfterWait("deletePostPage.deleteButton");
//        actions.clickElementAfterWait("deletePostPage.submitButton");
//    }
//    public void assertPostDeleted(String text) {
//
//        Assert.assertTrue("Post deleted successfully.", driver.getPageSource().contains(text));
//    }
//}