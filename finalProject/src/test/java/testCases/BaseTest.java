package testCases;

import com.telerikacademy.finalproject.weareapi.WeAreAPI;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseTest {
	public UserActions actions = new UserActions();
//	WeAreAPI weAreAPI = new WeAreAPI();

	@BeforeClass
	public static void setUp(){  UserActions.loadBrowser("base.url");	}

	@AfterClass
	public static void tearDown(){
		UserActions.quitDriver();
	}
}
