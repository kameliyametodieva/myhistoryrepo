package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

public class AdminPage extends BasePage {
    public AdminPage() {
        super("admin.url");
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
    }

    public String usernameUser = Utils.getUIMappingByKey("weAreAdmin.username.encoded");
    public String passwordUser = Utils.getUIMappingByKey("weAreAdmin.pass.encoded");

    public void updateFirstName(String firstName) {
        actions.clickElement("adminPage.goToAdminZoneButton");
        actions.clickElement("adminPage.viewUsers");
        actions.clickElement("adminPage.seeProfile");
        actions.clickElement("adminPage.editProfile");

        actions.isElementPresentUntilTimeout("userEditPage.firstName", 10);
        actions.clearField("userEditPage.firstName");
        actions.typeValueInField(firstName, "userEditPage.firstName");
    }
}