package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;

public class PostsPage extends BasePage {
    public PostsPage() {
        super("base.url");
    }

    public void openAllPublicPosts() {
        actions.waitFor(1000);
        actions.clickElementAfterWait("latestPosts.SignInButton");
    }

    public void ReadPublicPost() {
        actions.waitFor(1000);
        actions.clickElementAfterWait("latestPosts.SignInButton");
        actions.waitFor(1000);
        actions.clickElementAfterWait("explorePost.Button");
    }

    public void assertAllPublicPosts() {
        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("latestPosts.SignInButton");
    }

    public void assertReadPublicPost() {
        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("page.ReadPost");
    }
}


