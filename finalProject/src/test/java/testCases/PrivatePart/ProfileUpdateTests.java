package testCases.PrivatePart;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import testCases.BaseTest;

public class ProfileUpdateTests extends BaseTest {
    public String usernameUser= Utils.getUIMappingByKey("loginPage.userName");
    public String passwordUser=Utils.getUIMappingByKey("loginPage.password");
    public String upName= Utils.getUIMappingByKey("userUpdatePage.firstName");
    public String upLastName= Utils.getUIMappingByKey("userUpdatePage.lastName");


    private final UserUpdatePage userUpdatePage = new UserUpdatePage();
    private final LogInPage logInPage = new LogInPage();





    @Before
    public void preconditions() {
        logInPage.navigateToPage();
        logInPage.logIn(usernameUser, passwordUser);
        userUpdatePage.navigateToPage();
    }

    @Test
    public void SU_02_004_SuccessfullyUpdateUserByFirstName() {
        userUpdatePage.updateFirstName(upName);
        userUpdatePage.saveButton();
        userUpdatePage.assertFirstNameIsChanged();
//        actions.waitMillis(1000);

    }
    @Test
    public void SU_02_005_SuccessfullyUpdateUserByLastName() {
        userUpdatePage.updateLastName(upLastName);
        userUpdatePage.saveButton();
        userUpdatePage.assertLastNameIsChanged();
        actions.waitMillis(1000);

    }


}

