package testCases.PrivatePart;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.LogOutPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import testCases.BaseTest;


public class LogInLogOutTests extends BaseTest {


    public String  errorMessage = Utils.getUIMappingByKey("loginPage.errorMassage");
    private final NavigationPage navigationPage = new NavigationPage();
    private final LogInPage logInPage = new LogInPage();
    private final LogOutPage logOutPage = new LogOutPage();
    public String usernameUser= Utils.getUIMappingByKey("loginPage.userName");
    public String passwordUser=Utils.getUIMappingByKey("loginPage.password");
    public String invalidPasswordUser=Utils.getUIMappingByKey("loginPage.invalidPassword");



    @Before
    public void preconditions() {
        logInPage.navigateToPage();
    }

    @Test
    public void SU_02_02_UnsuccessfulLogInWithInvalidPassword() {
        logInPage.logIn(usernameUser, invalidPasswordUser);
        logInPage.assertWrongPasswordMessage(errorMessage);

//        actions.waitMillis(1000);
    }

    @Test
    public void SU_02_01_SuccessfulLogInWithValidCredentials() {
        logInPage.logIn(usernameUser, passwordUser);
        logInPage.assertLoginSuccessful();
        actions.waitMillis(1000);

    }

    @Test
    public void SU_02_19_SuccessfulLogOut() {
        logInPage.logIn(usernameUser, passwordUser);
        logOutPage.logOut();
        navigationPage.assertPageNavigated();
        actions.waitMillis(1000);

    }
}